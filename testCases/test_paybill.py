import pytest 
from selenium import webdriver
from pageObjects.LoginPage import LoginPage
import os
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
from pageObjects.PayBill import PayBill
import time

class Test_003_PayBill:
    baseURL = ReadConfig.getapplicationurl()
    username = ReadConfig.getusername()
    password = ReadConfig.getpassword()
    logger = LogGen.loggen()

    def test_paysubmit(self, setup):
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setusername(self.username)
        self.lp.setpassword(self.password)
        self.lp.clicklogin()
        actual_title = self.driver.title
        if actual_title == 'PayApp - Dashboard':
            assert True
            self.logger.info("************ Login data validated ************")
            self.pb = PayBill(self.driver)
            self.pb.clicktopay()
            actual_title = self.driver.title
            if actual_title == 'PayApp - Bill pay':
                assert True
                self.logger.info("************ Pay Bill Page validated ************")
                self.pb.setnrbill("1234")
                self.pb.setbuyer("Chihaia Sorin")
                self.pb.setclientid("280391")
                self.pb.setamount("1299.99")
                self.pb.setphone("0741932994")
                self.pb.submitpay()
                self.msg = self.driver.find_element_by_tag_name("body").text
                if 'Plata a fost inregistrata cu success pentru factura cu numarul:' in self.msg:
                    assert True
                    self.logger.info ("************ Payment test passed ************")
                else:
                    self.driver.save_screenshot(".\\Screenshots\\" + "tes_paybill.png")
                    self.logger.info("************ Payment test failed ************")
                    assert False
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_login_pay.png")
            self.logger.error("*********** Login Test Failed ***********")
            self.driver.close()
            assert False
    
