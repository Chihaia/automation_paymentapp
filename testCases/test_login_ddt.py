import pytest 
from selenium import webdriver
from pageObjects.LoginPage import LoginPage
import os
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
from utilities import XLUtils
import time


class Test_002_DDT_Login:
    baseURL = ReadConfig.getapplicationurl()
    path=".//TestData/LoginData.xlsx"
    logger = LogGen.loggen()
    
    def test_login_ddt(self, setup):
        self.logger.info("*********** Test_002_DDT_Login ***********")
        self.logger.info("*********** Verifying Login Test ***********")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)

        self.rows = XLUtils.getRowCount(self.path,'Sheet1')
        print("Number of Rows in a Excel:", self.rows)

        lst_status = []

        for r in range (2,self.rows+1):
            self.user = XLUtils.readData(self.path, 'Sheet1', r, 1)
            self.password = XLUtils.readData(self.path,'Sheet1',r, 2)
            self.exp = XLUtils.readData(self.path,'Sheet1',r, 3)

            self.lp.setusername(self.user)
            self.lp.setpassword(self.password)
            self.lp.clicklogin()
            time.sleep(5)

            act_title = self.driver.title
            exp_title = 'PayApp - Dashboard'

            if act_title == exp_title:
                if self.exp == "Pass":
                    self.logger.info("*********** Passed ***********")
                    self.lp.clicklogout()
                    self.lp.click_login_alo()
                    lst_status.append("Pass")
                elif self.exp == "Fail":
                    self.logger.info("*********** Failed ***********")
                    lst_status.append("Fail")
            elif act_title != exp_title:
                if self.exp == "Pass":
                    self.logger.info("*********** Failed ***********")
                    lst_status.append("Fail")
                elif self.exp == "Fail":
                    self.logger.info("*********** Passed ***********")
                    lst_status.append("Pass")   
        if "Fail" not in lst_status:
            self.logger.info("*********** Login DDT test passed ***********")
            self.driver.close()
            assert True
        else:
            self.logger.info("*********** Login DDT test FAILED ***********")
            self.driver.close()
            assert False

            self.logger.info("*********** End of Login DDT Test ***********")
            self.logger.info("*********** Completed TC_LoginDDT_002 ***********")
