import pytest 
from selenium import webdriver
from pageObjects.LoginPage import LoginPage
import os
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen


class Test_001_Login:
    baseURL = ReadConfig.getapplicationurl()
    username = ReadConfig.getusername()
    password = ReadConfig.getpassword()

    logger = LogGen.loggen()

    def test_homepagetitle(self, setup):
        self.logger.info("*********** Test_001_Login ***********")
        self.logger.info("*********** Verifying Home Page Title ***********")
        self.driver = setup
        self.driver.get(self.baseURL)
        actual_title = self.driver.title
        if actual_title == 'PayApp - Login':
            assert True
            self.driver.close()
            self.logger.info("*********** Home Page Title is passed ***********")
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_homepagetitle.png")
            self.driver.close()
            self.logger.error("*********** Home Page Title is failed ***********")
            assert False
    
    def test_login(self, setup):
        self.logger.info("*********** Verifying Login Test ***********")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setusername(self.username)
        self.lp.setpassword(self.password)
        self.lp.clicklogin()
        actual_title = self.driver.title
        if actual_title == 'PayApp - Dashboard':
            assert True
            self.logger.info("*********** Login Test Passed ***********")
            self.driver.close()
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_login.png")
            self.logger.error("*********** Login Test Failed ***********")
            self.driver.close()
            assert False
