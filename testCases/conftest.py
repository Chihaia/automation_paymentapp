from selenium import webdriver
import pytest 


@pytest.fixture()
def setup(browser):
    if browser == 'chrome':
        driver = webdriver.Chrome()
    elif browser == 'firefox':
        driver = webdriver.Firefox()
    else:
        driver=webdriver.Ie()
    return driver

# pass the desired browser argument to run the tests
def pytest_addoption(parser):
    parser.addoption("--browser")

@pytest.fixture()
def browser(request):
    return request.config.getoption('--browser')

# PyTest HTML Report Generator

# It is for Adding Environment info to HTML Report
def pytest_configure(config):
    config._metadata['Project Name'] = 'App Payment'
    config._metadata['Module Name'] = 'Customers'
    config._metadata['Tester'] = 'Sorin'

# It is hook for delete/Modify Environment info to HTML Report
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME", None)
    metadata.pop("Plugins", None)