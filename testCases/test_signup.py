import pytest 
from selenium import webdriver
from pageObjects.SignupPage import SignUp
import os
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
import string
import random
import time


class Test_002_SignUp:
    baseURL = ReadConfig.getapplicationurl()
    logger = LogGen.loggen()

    def test_signuplink(self, setup):
        self.logger.info("************ Test_002_SignUP ************")
        self.logger.info("************ Verifying SignUp Page Title ************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.driver.maximize_window()
        self.sp = SignUp(self.driver)
        self.sp.clickregister()
        actual_title = self.driver.title
        if actual_title == 'PayApp - Register':
            assert True
            self.driver.close()
            self.logger.info("************ Sign UP page title validated ************")
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_signuppagetitle.png")
            self.driver.close()
            self.logger.error("*********** SignUp Page Title is failed ***********")
            assert False
        self.logger.info("*********** Checking signup page test ended ***********")

    def test_createuser(self, setup):
        self.logger.info("*********** Creating a new user ***********")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.driver.maximize_window()
        self.sp = SignUp(self.driver)
        self.sp.clickregister()
        self.email = randomgenerator() + '@gmail.com'
        self.sp.setemail(self.email)
        self.sp.setnume('TestNume')
        self.sp.setprenume('TestPrenume')
        self.sp.setphone('0741991991')
        self.sp.setpassword1('Tests123123')
        self.sp.setpassword2('Test123123')
        time.sleep(5)
        self.sp.clicksignup()
        time.sleep(5)
        self.logger.info('*********** Saving the new user ***********')
        self.logger.info('*********** Add new user validation started ***********')

        self.msg = self.driver.find_element_by_tag_name("body").text

        if 'Contul a fost creat, verificati adresa de email pentru activare' in self.msg:
            assert True
            self.logger.info("*********** Add new user test passed ***********")
        else:
            self.driver.save_screenshot(".\\Screenshots\\" + "test_registeruser.png")
            self.logger.info("*********** Add new user test failed ***********")
            assert False

        self.driver.close()
        self.logger.info("*********** Creating a new user test ended ***********")

def randomgenerator(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))