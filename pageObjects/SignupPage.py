from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#create a new user in the paymentapp
class SignUp:
    textbox_email = "email"
    textbox_nume = "nume"
    textbox_prenume = "prenume"
    textbox_telefon = "telefon"
    textbox_password1 = "password1"
    textbox_password2 = "password2"
    button_signup = "signup"
    link_register = "/html/body/div/div/form/small/a[1]"

    def __init__(self, driver):
        self.driver = driver

    def setemail(self, email):
        self.driver.find_element_by_name(self.textbox_email).clear()
        self.driver.find_element_by_name(self.textbox_email).send_keys(email)

    def setnume(self, nume):
        self.driver.find_element_by_name(self.textbox_nume).clear()
        self.driver.find_element_by_name(self.textbox_nume).send_keys(nume)

    def setprenume(self, prenume):
        self.driver.find_element_by_name(self.textbox_prenume).clear()
        self.driver.find_element_by_name(self.textbox_prenume).send_keys(prenume)

    def setphone(self, telefon):
        self.driver.find_element_by_name(self.textbox_telefon).clear()
        self.driver.find_element_by_name(self.textbox_telefon).send_keys(telefon)

    def setpassword1(self, password1):
        self.driver.find_element_by_name(self.textbox_password1).clear()
        self.driver.find_element_by_name(self.textbox_password1).send_keys(password1)

    def setpassword2(self, password2):
        self.driver.find_element_by_name(self.textbox_password2).clear()
        self.driver.find_element_by_name(self.textbox_password2).send_keys(password2)

    def clickregister(self):
        self.driver.find_element_by_xpath(self.link_register).click()

    def clicksignup(self):
        self.driver.find_element_by_name(self.button_signup).click()
