from selenium import webdriver
from selenium.webdriver.common.keys import Keys

#make a bill payment
class PayBill:
    textbox_nrbill = "numarfact"
    textbox_buyer = "cumparator"
    textbox_clientid = "clientid"
    textbox_amount = "suma"
    textbox_phone = "telefon"
    link_submit = "/html/body/div/div/div/div/form/div/button"
    link_pay = "/html/body/div/div/div/div/div[1]/div/a"
    link_history = "/html/body/div/div/div/div/div[2]/div/a"

    def __init__(self, driver):
        self.driver = driver

    def setnrbill(self, numarfact):
        self.driver.find_element_by_name(self.textbox_nrbill).clear()
        self.driver.find_element_by_name(self.textbox_nrbill).send_keys(numarfact)

    def setbuyer(self, cumparator):
        self.driver.find_element_by_name(self.textbox_buyer).clear()
        self.driver.find_element_by_name(self.textbox_buyer).send_keys(cumparator)

    def setclientid(self, clientid):
        self.driver.find_element_by_name(self.textbox_clientid).clear()
        self.driver.find_element_by_name(self.textbox_clientid).send_keys(clientid)

    def setamount(self, amount):
        self.driver.find_element_by_name(self.textbox_amount).clear()
        self.driver.find_element_by_name(self.textbox_amount).send_keys(amount)

    def setphone(self, phone):
        self.driver.find_element_by_name(self.textbox_phone).clear()
        self.driver.find_element_by_name(self.textbox_phone).send_keys(phone)

    def clicktopay(self):
        self.driver.find_element_by_xpath(self.link_pay).click()

    def submitpay(self):
        self.driver.find_element_by_xpath(self.link_submit).click()

    def historypayments(self):
        self.driver.find_element_by_xpath(self.link_history).click()

    