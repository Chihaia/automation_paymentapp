from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class LoginPage:
    textbox_username_name = "username"
    textbox_password_name = "password"
    button_login_name = "login"
    link_logout_linktest = "/html/body/div/div/header/nav/div/div/p[3]/a"
    login_after_logout = "/html/body/small/a"

    def __init__(self, driver):
        self.driver = driver

    def setusername(self, username):
        self.driver.find_element_by_name(self.textbox_username_name).clear()
        self.driver.find_element_by_name(self.textbox_username_name).send_keys(username)

    def setpassword(self, password):
        self.driver.find_element_by_name(self.textbox_password_name).clear()
        self.driver.find_element_by_name(self.textbox_password_name).send_keys(password)

    def clicklogin(self):
        self.driver.find_element_by_name(self.button_login_name).click()

    def clicklogout(self):
        self.driver.find_element_by_xpath(self.link_logout_linktest).click()

    def click_login_alo(self):
        self.driver.find_element_by_xpath(self.login_after_logout).click()